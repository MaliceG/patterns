#ifndef SUBSCRIBER_H
#define SUBSCRIBER_H

#include <QString>

class Subscriber
{
public:
    Subscriber();
    virtual void update(QString message) = 0;
};

class TestSubscriber : public Subscriber
{
public:
    virtual void update(QString message);
};

class AnotherSubscriber : public Subscriber
{
public:
    virtual void update(QString message);
};

#endif // SUBSCRIBER_H
