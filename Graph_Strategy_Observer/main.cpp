#include <QCoreApplication>

#include <QVector>
#include <QList>
#include "graph.h"
#include <QDebug>
#include "subscriber.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QVector<QList<int>> baseGraph;
    baseGraph.push_back(QList<int>() << 1 << 2 << 3); //0
    baseGraph.push_back(QList<int>() << 0 << 2); //1
    baseGraph.push_back(QList<int>() << 0 << 1 << 4); //2
    baseGraph.push_back(QList<int>() << 0); //3
    baseGraph.push_back(QList<int>() << 2); //4

    Graph graph(baseGraph);
    int num = 0;
    for (auto &root : graph.getGraph())
        qDebug() << num++ << root;

    TestSubscriber sub1;
    AnotherSubscriber sub2;
    graph.addSubscriber(&sub1);
    graph.addSubscriber(&sub2);

    graph.search(new SearchStrategyBreadth());
    graph.search(new SearchStrategyDepth());

    return a.exec();
}
