#include "searchstrategy.h"

#include <QDebug>

SearchStrategy::SearchStrategy()
{

}

bool SearchStrategy::prepareForSearch(QVector<QList<int> > *graph)
{
    this->graph = graph;

    visitedRoots.clear();
    visitedRoots.fill(false, graph->count());

    if (graph->isEmpty())
    {
        qDebug() << "Graph is empty!";
        return false;
    }

    return true;
}

void SearchStrategyDepth::search(QVector<QList<int>> *graph)
{
    if (!prepareForSearch(graph))
        return;

    deapthSearch(0);
}

void SearchStrategyDepth::deapthSearch(int rootNumber)
{
    qDebug() << rootNumber;
    visitedRoots[rootNumber] = true;

    for (auto childNumber : graph->at(rootNumber))
    {
        if (!visitedRoots[childNumber])
            deapthSearch(childNumber);
    }
}

void SearchStrategyBreadth::search(QVector<QList<int>> *graph)
{
    if (!prepareForSearch(graph))
        return;

    QQueue<int> queue;
    queue.push_back(0);
    visitedRoots[0] = true;

    while (!queue.isEmpty())
    {
        int rootNumber = queue.takeFirst();
        qDebug() << rootNumber;

        for (auto childNumber : graph->at(rootNumber))
        {
            if (!visitedRoots[childNumber])
            {
                queue.push_back(childNumber);
                visitedRoots[childNumber] = true;
            }
        }
    }
}
