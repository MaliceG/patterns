#ifndef SEARCHSTRATEGY_H
#define SEARCHSTRATEGY_H

#include <QVector>
#include <QQueue>
#include <QList>

class SearchStrategy
{
public:
    SearchStrategy();
    virtual void search(QVector<QList<int>> *graph) = 0;
    bool prepareForSearch(QVector<QList<int> > *graph);
protected:
    QVector<int> visitedRoots;
    QVector<QList<int>> *graph;
};

class SearchStrategyDepth : public SearchStrategy
{
public:
    virtual void search(QVector<QList<int> > *graph);
    void deapthSearch(int rootNumber);
};

class SearchStrategyBreadth : public SearchStrategy
{
public:
    virtual void search(QVector<QList<int> > *graph);
};

#endif // SEARCHSTRATEGY_H
