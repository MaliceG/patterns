#ifndef GRAPH_H
#define GRAPH_H

#include <QVector>
#include <QList>

#include "searchstrategy.h"
#include "subscriber.h"

class Graph
{
public:
    Graph();
    Graph(QVector<QList<int>> graph);
    void initGraph(QVector<QList<int>> graph);

    QVector<QList<int> > getGraph();

    virtual void search(SearchStrategy *strategy);

    void addSubscriber(Subscriber *sub);
private:
    QVector<QList<int>> graph;

    QList<Subscriber*> subscribersList;
    void notifySubscribers();
};

#endif // GRAPH_H
