#include "graph.h"

#include <QDebug>
#include <QQueue>

Graph::Graph()
{

}

Graph::Graph(QVector<QList<int>> graph)
{
    this->graph = graph;
}

void Graph::initGraph(QVector<QList<int> > graph)
{
    this->graph = graph;
}

void Graph::search(SearchStrategy *strategy)
{
    qDebug() << "Start searching";

    strategy->search(&graph);

    notifySubscribers();

    qDebug() << "End searching\n";
}

void Graph::addSubscriber(Subscriber *sub)
{
    subscribersList.append(sub);
}

void Graph::notifySubscribers()
{
    for (auto subscriber : subscribersList)
        subscriber->update("Searching is over!");
}

QVector<QList<int>> Graph::getGraph()
{
    return graph;
}
