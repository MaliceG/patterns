#include "subscriber.h"
#include <QDebug>

Subscriber::Subscriber()
{

}

void TestSubscriber::update(QString message)
{
    qDebug() << "TestSubscriber: " << message;
}

void AnotherSubscriber::update(QString message)
{
    qDebug() << "Another subscriber: " << message;
}
