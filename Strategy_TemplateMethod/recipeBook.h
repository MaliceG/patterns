#ifndef RECIPEBOOK_H
#define RECIPEBOOK_H

#include <iostream>
#include "sandwich.h"

class Strategy
{
public:
    virtual void showRecipe() = 0;
};

class StrategyDefault : public Strategy
{
    void showRecipe()
    {
        std::cout << "Default strategy\n\n";
    }
};

class StrategyMeat : public Strategy
{
    void showRecipe()
    {
        std::cout << "Meat strategy:\n";
        MeatSandwich sandwich;
        sandwich.makeSandwich();
    }
};

class StrategyVegan : public Strategy
{
    void showRecipe()
    {
        std::cout << "Vegan strategy:\n";
        VeganSandwich sandwich;
        sandwich.makeSandwich();
    }
};

class RecipeBook
{
public:
    RecipeBook(Strategy *strategy = new StrategyDefault()) : strategy(strategy) {}

    void showRecipe() {strategy->showRecipe();}
    void setStrategy(Strategy *strategy) {this->strategy = strategy;}

private:
    Strategy *strategy;
};


#endif // RECIPEBOOK_H
