#include <QCoreApplication>

#include "recipeBook.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    StrategyMeat  meatBurger;
    StrategyVegan veganBurger;

    RecipeBook book;
    book.showRecipe();

    book.setStrategy(&meatBurger);
    book.showRecipe();

    book.setStrategy(&veganBurger);
    book.showRecipe();

    return a.exec();
}
