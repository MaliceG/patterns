#ifndef SANDWICH_H
#define SANDWICH_H

#include <iostream>


class Sandwich
{
public:
    Sandwich() {}

    void makeSandwich()
    {
        cutBread();

        addMeat();
        addCheese();
        addVegetables();

        wrapSandwich();
    }

    void cutBread() { std::cout << "Cutting bread\n"; }
    void wrapSandwich() { std::cout << "Wrapping sandwich\n\n"; }

    virtual void addMeat() = 0;
    virtual void addCheese() = 0;
    virtual void addVegetables() = 0;

};

class VeganSandwich : public Sandwich
{
public:
    void addMeat() { std::cout << "Adding sour meat\n"; }
    void addCheese() { std::cout << "Adding cheese\n"; }
    void addVegetables() { std::cout << "Adding cucumber, onion, tomato rings\n"; }
};

class MeatSandwich : public Sandwich
{
public:
    void addMeat() { std::cout << "Adding meat\n"; }
    void addCheese() { std::cout << "Adding cheese\n"; }
    void addVegetables() { std::cout << "Adding sweet pepper, cucumber, tomato rings, salad\n"; }
};

#endif // SANDWICH_H
